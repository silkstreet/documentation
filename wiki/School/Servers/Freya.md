# Freya
## Info
### Software
- Kea DHCP Server
### Network
#### Interfaces
##### ens192
- IP: 10.13.33.5/24
### HW Config
- OS: Ubuntu 22.04
- CPU: 2
- RAM: 4GB
- Disks:
	- OS: 16GB
## Setup
### Kea install

```bash
apt install kea
# Edit config accordingly to network information
# /etc/kea/kea-dhcp4.conf
systemctl restart kea-dhcp4-server
```

```json
{
    "Dhcp4": {
        "interfaces-config": {
            "interfaces": [
                "ens192"
            ]
        },
        "control-socket": {
            "socket-type": "unix",
            "socket-name": "/tmp/kea4-ctrl-socket"
        },
        "lease-database": {
            "type": "memfile",
            "lfc-interval": 3600
        },
        "expired-leases-processing": {
            "reclaim-timer-wait-time": 10,
            "flush-reclaimed-timer-wait-time": 25,
            "hold-reclaimed-time": 3600,
            "max-reclaim-leases": 100,
            "max-reclaim-time": 250,
            "unwarned-reclaim-cycles": 5
        },
        "renew-timer": 900,
        "rebind-timer": 1800,
        "valid-lifetime": 3600,
        "option-data": [
            {
                "name": "domain-name-servers",
                "data": "1.1.1.1, 1.0.0.1"
            }
        ],
        "subnet4": [
            {
                "subnet": "10.13.33.0/24",
                "pools": [
                    {
                        "pool": "10.13.33.50 - 10.13.33.200"
                    }
                ],
                "option-data": [
                    {
                        "name": "routers",
                        "data": "10.13.33.1"
                    }
                ]
            }
        ],
        "loggers": [
            {
                "name": "kea-dhcp4",
                "output_options": [
                    {
                        "output": "/var/log/kea-dhcp4.log"
                    }
                ],
                "severity": "INFO",
                "debuglevel": 0
            }
        ]
    }
}
```