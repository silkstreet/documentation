# Jotunheim
## Info
### Roles
- AD-Domain-Services
	- DNS
### Network
- IP: 10.13.33.10/24
### HW Config
- OS: Win2022(Core)
- CPU: 2
- RAM: 4GB
- Disks:
	- OS: 90GB
## Setup
```powershell
Install-WindowsFeature AD-Domain-Services -IncludeAllSubFeature -IncludeManagementTools
Install-ADDSForest -DomainName bifrost.lan -InstallDns
```

